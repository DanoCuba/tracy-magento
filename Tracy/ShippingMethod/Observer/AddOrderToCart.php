<?php

namespace Tracy\ShippingMethod\Observer;

class AddOrderToCart implements \Magento\Framework\Event\ObserverInterface
{
	public function execute(\Magento\Framework\Event\Observer $observer)
	{
	   $writer = new \Zend\Log\Writer\Stream(BP.'/var/log/test.log'); // creates new log file named test.log in var/log folder
       $logger = new \Zend\Log\Logger();
       $logger->addWriter($writer);
       $logger->info("Aquí caputré el evento: add_order_cart."); // this will be print in test.log file
	}
}