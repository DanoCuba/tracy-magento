<?php

namespace Tracy\ShippingMethod\Observer;

class SendOrderToTracy implements \Magento\Framework\Event\ObserverInterface
{
	private $orderFactory;
	private $logger;
	
	function __construct() {
        $this->orderFactory = \Magento\Framework\App\ObjectManager::getInstance();
		$writer = new \Zend\Log\Writer\Stream(BP.'/var/log/test.log'); // creates new log file named test.log in var/log folder
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }
	
	public function execute(\Magento\Framework\Event\Observer $observer)
	{
       $this->logger->info("Aquí caputré el evento: checkout_onepage_controller_success_action."); // this will be print in test.log file
	   
	   $order_ids  = $observer->getEvent()->getOrderIds();
	   $order_id   = $order_ids[0];
	   $order = $this->orderFactory->get('\Magento\Sales\Model\Order')->load($order_id);
	   
	   $shipping_method = $order->getShippingDescription();
	   
	   if (strpos($shipping_method, 'Tracy') !== false) {
			$orderToTracy = $this->generate_tracy_order($order);
			//$this->debug_to_console(json_encode($orderToTracy));
			$this->tracy_send_confirmation_order_notification($orderToTracy);
	   }
	}
	
	public function debug_to_console($data) {
        $output = $data;
        if ( is_array( $output ) )
            $output = implode( ',', $output);

        echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
    }
	
	public function get_config_data($key)
	{
		$config = $this->orderFactory->get('Magento\Framework\App\Config\ScopeConfigInterface');
		$shippingKey = 'carriers/shippingmethod/' . $key;
		return $config->getValue($shippingKey);
	}
	
	public function generate_tracy_order($order)
	{
		$shippingAddress = $order->getShippingAddress();
		
		$data_array =  array(
			 "group_ref"=> "",
			 "shipper_company_uuid"=> $this->get_config_data("shipper_company_id"),
			 "delivery_address"=> $shippingAddress->getData("street"),
			 "delivery_cp"=> $shippingAddress->getData("postcode"),
			 "delivery_province"=> $shippingAddress->getData("region"),
			 "delivery_partido"=> "",
			 "delivery_locality"=> $shippingAddress->getData("city"),
			 "client_email"=> $shippingAddress->getData("email"),
			 "client_cel"=> $shippingAddress->getData("telephone"),
			 "contact_first_name"=> $shippingAddress->getData("firstname").' '.$shippingAddress->getData("lastname"),
			 "contact_cel"=> $shippingAddress->getData("telephone"),
			 "products"=> []
		 );
		 
		 $items = $order->getItemsCollection();	
		 foreach($items as $cart_item) { 
				$data_array['products'][] = array(
					"quantity"   => $cart_item->getQtyOrdered(),
					"unit"       => "Units",
					"product"    => $cart_item->getName(),
					"weight"     => $cart_item->getWeight(),
					"weight_vol" => '' // calcular con dimensiones?
				);			
			}  
		 
		 return $data_array;
	}
	
	public function tracy_send_confirmation_order_notification($orderToTracy) 
	{		 	    		
		$apiKey =  ($this->get_config_data("api_key") !== null) ? $this->get_config_data("api_key") : '';
		$apiSecret = ($this->get_config_data("api_secret") !== null) ? $this->get_config_data("api_secret") : '';
		$apiURL = ($this->get_config_data("api_url") !== null) ? $this->get_config_data("api_url") : 'http://admin.tracy.io/orders/';
					
		 $query = '?';
		 if($apiKey != '')
			 $query = $query.'api_key='.$apiKey;
		 if($apiSecret != ''){
			 if($query != '?')
				$query = $query.'&';
			 $query = $query.'api_secret='.$apiSecret;
		 }
		
		$make_call = $this->callAPI('POST', $apiURL.'/confirm_order/'.$query, json_encode($orderToTracy));
			
		if($make_call){
			$response = json_decode($make_call, true);
		}	
	}
	
	public function callAPI($method, $url, $data){
		$curl = curl_init();

		switch ($method){
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);
				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
					break;
		 	case "PUT":
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
				if ($data)
					curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
					break;
			default:
				if ($data)
					$url = sprintf("%s?%s", $url, http_build_query($data));
		}

		// OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

		// EXECUTE:
		$result = curl_exec($curl);
		if(!$result){ echo "Connection Failure"; return;}
		curl_close($curl);
		return $result;
	}
}